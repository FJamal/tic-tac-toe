<?php
/*$board = [["X", "X", "O"],
        ["X", "", "O"],
        ["O", "", ""]];*/

        $board = [["", "", ""],
                ["", "", ""],
                ["", "", ""]];



function isboardfull($arr)
{
    $rows = count($arr);
    $col = count($arr[0]);

    //the number of empty spaces this board should have
    $emptyspaces = $rows * $col;
    //count for counting empty spaces
    $count = 0;
    foreach($arr as $row)
    {
        for($i = 0; $i < $col; $i++)
        {
            if(!empty($row[$i]))
            {
                $count += 1;
            }
        }
    }

    if($count < $emptyspaces)
    {
        return true;
    }
    else
    {
        return false;
    }
}

//returns an array of array of indexex for empty spaces like [[row,col]]
function getemptyspaces($arr)
{
    //indexes to be stored as [row, column]
    $spaceindexes = [];
    $rows = count($arr);
    $columns = count($arr[0]);

    for($i = 0; $i < $rows; $i++)
    {
        for($j = 0; $j < $columns; $j++)
        {
            if(empty($arr[$i][$j]))
            {
                //save the index of empty space
                $spaceindexes[] = [$i, $j];
            }
        }
    }

    return $spaceindexes;
}

//function to find if player has a winning line
function evaluate($board)
{
    //checking for winning rows
    for($row = 0; $row < 3; $row++)
    {
        if($board[$row][0] == $board[$row][1] && $board[$row][1] == $board[$row][2])
        {
            if($board[$row][0] == "X")
            {
                //print("in horizontal");
                return 10;
            }
            else if($board[$row][0] == "O")
            {
                return -10;
            }
        }
    }
    //checking for vertical lines
    for($col = 0; $col < 3; $col++)
    {
        if($board[0][$col] == $board[1][$col] && $board[1][$col] == $board[2][$col])
        {
            if($board[0][$col] == "X" )
            {
                //print("in vertical");
                return 10;
            }
            else if($board[0][$col] == "O")
            {
                return -10;
            }
        }
    }
    //checking for daignols
    if($board[0][0] == $board[1][1] && $board[1][1] == $board[2][2])
    {
        if($board[0][0] == "X" )
        {
            //print("in diagnol");
            return 10;
        }
        else if($board[0][0] == "O")
        {
            return -10;
        }
    }
    if($board[0][2] == $board[1][1] && $board[2][0] == $board[1][1])
    {
        if($board[0][2] == "X")
        {
            //print("other diagnol");
            return 10;
        }
        else if($board[0][2] == "O")
        {
            return -10;
        }
    }

    return 0;
}


function minimax($board, $turn)
{
    $game = $board;
    // get the score of current board
    $score = evaluate($game);

    if($score == 10)
    {
        return $score;
    }
    /*IF BUGS TRY REMOVING THIS FIRST*/
    if($score == -10)
    {
        return $score;
    }
    /*END BUg FIX*/
    if(count(getemptyspaces($game)) === 0)
    {
        return 0;
    }

    if($turn == "X")
    {
        $availablemoves = getemptyspaces($game);
        $best = -1000;
        foreach ($availablemoves as $move)
        {
            $game[$move[0]][$move[1]] = $turn;
            $best = max($best, minimax($game, "O"));

            //resetting the board
            $game = $board;
        }

        return $best;
    }
    else if($turn == "O")
    {
        $availablemoves = getemptyspaces($game);
        $best = 1000;
        foreach ($availablemoves as $move)
        {
            $game[$move[0]][$move[1]] = $turn;
            $best = min($best, minimax($game, "X"));

            //resetting the board
            $game = $board;
        }

        return $best;
    }
}

function bestmove($board, $turn)
{
    $game = $board;
    if($turn == "X")
    {
        $bestval = -1000;
        $bestmove;
        $availablemoves = getemptyspaces($board);

        foreach($availablemoves as $move)
        {
            $game[$move[0]][$move[1]] = $turn;
            $movevalue = minimax($game, "O");

            //reset board
            $game = $board;

            if($movevalue > $bestval)
            {
                $bestval = $movevalue;
                $bestmove = $move;
            }
        }

        return $bestmove;
    }
    else if($turn == "O")
    {
        $bestval = 1000;
        $bestmove;
        $availablemoves = getemptyspaces($board);

        foreach($availablemoves as $move)
        {
            $game[$move[0]][$move[1]] = $turn;
            $movevalue = minimax($game, "X");
            //reset board
            $game = $board;

            if($movevalue < $bestval)
            {
                $bestval = $movevalue;
                $bestmove = $move;
            }
        }

        return $bestmove;
    }
}






while(isboardfull($board) )
{
    //display board
    for($i = 0; $i < 3; $i++)
    {
        for($j = 0; $j < 3; $j++)
        {
            if(empty($board[$i][$j]))
            {
                print("-  ");
            }
            else
            {
                print($board[$i][$j] . "  ");
            }
        }
        print("\n");
    }
    print("Input:");
    $input= trim(fgets(STDIN));
    $move = str_split($input);

    $board[$move[0]][$move[1]] = "X";
    //print(winning($board, "X"));
    if(evaluate($board, "X") == 10 || isboardfull($board) == false)
    {
        print_r($board);
        break;
    }

    //TEST cpu
    $cpu = bestmove($board, "O");
    $board[$cpu[0]][$cpu[1]] = "O";
    //print_r($board);


}

//print_r(bestmove($board, "O"));
//print(evaluate($board, "X"));



/*FOR if cpu has 1st try
while(isboardfull($board) )
{

    //TEST cpu
    $cpu = bestmove($board, "O");
    $board[$cpu[0]][$cpu[1]] = "O";

    //display board
    for($i = 0; $i < 3; $i++)
    {
        for($j = 0; $j < 3; $j++)
        {
            if(empty($board[$i][$j]))
            {
                print("-  ");
            }
            else
            {
                print($board[$i][$j] . "  ");
            }
        }
        print("\n");
    }
    print("Input:");
    $input= trim(fgets(STDIN));
    $move = str_split($input);

    $board[$move[0]][$move[1]] = "X";
    //print(winning($board, "X"));
    if(evaluate($board, "X") == 10 || isboardfull($board) == false)
    {
        print_r($board);
        break;
    }


    //print_r($board);


}*/





?>
