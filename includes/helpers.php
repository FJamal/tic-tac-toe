<?php
    function redirect($page)
    {
        /* Redirect to a different page in the current directory that was requested */
        $host  = $_SERVER['HTTP_HOST'];
        $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
        $extra = $page;
        header("Location: http://$host$uri/$extra");
        exit;
    }

    /*This function figures out if X or O has made a line
    and the game is finished and used to stylize winning line
    returns: location index where the winning line starts from based on starting
            starting index in case of horizontal or vertical line
            and if its a cross then its either the first row index if line is [0][0][1][1][2][2]
            or 2 incase of cross in [2][0][1][1][0][1]
            Line format if its a vertical line, horizontal or a cross
            returns key finished as true if game is finished */
    function isgamefinished()
    {
        for($i = 0; $i < 3; $i++)
        {
            //checking winning line horizontally or vertically
            if($_SESSION["board"][$i][2] !== "None" && $_SESSION["board"][$i][1] !== "None" && $_SESSION["board"][$i][0] !== "None" && $_SESSION["board"][$i][0] == $_SESSION["board"][$i][1] && $_SESSION["board"][$i][0] == $_SESSION["board"][$i][2] )
            {
                /*print("WON");
                print("line format is horizontal\n");
                print("position: {$i}");*/
                return ["finished" => True,
                        "line" => "horizontal",
                        "location" => $i,
                        "winner" => $_SESSION["board"][$i][2]];

            }
            if($_SESSION["board"][2][$i] !== "None" && $_SESSION["board"][1][$i] !== "None" && $_SESSION["board"][0][$i] !== "None" && $_SESSION["board"][0][$i] == $_SESSION["board"][1][$i] && $_SESSION["board"][0][$i] == $_SESSION["board"][2][$i])
            {
                //print("WON");
                return ["finished" => True,
                        "line" => "vertical",
                        "location" => $i,
                        "winner" => $_SESSION["board"][2][$i]];
            }
        }

        if($_SESSION["board"][0][0] !== "None" && $_SESSION["board"][1][1] !== "None"
            && $_SESSION["board"][2][2] !== "None" && $_SESSION["board"][0][0] == $_SESSION["board"][1][1]
            && $_SESSION["board"][2][2] == $_SESSION["board"][0][0])
        {
            return ["finished" => True,
                    "line" => "X",
                    "location" => 0,
                    "winner" => $_SESSION["board"][0][0]];
        }

        if($_SESSION["board"][2][0] !== "None" && $_SESSION["board"][1][1] !== "None"
            && $_SESSION["board"][0][2] !== "None" && $_SESSION["board"][2][0] == $_SESSION["board"][1][1]
            && $_SESSION["board"][2][0] == $_SESSION["board"][0][2] )
        {
            return ["finished" => True,
                    "line" => "X",
                    "location" => 2,
                    "winner" => $_SESSION["board"][2][0]];
        }

        return false;
    }

    /*Expects $sybmol = "X" or "O"
    Put a "X" or "O" in session["board"] on cpu behalf
    modifiy $_SESSION["turn"]*/
    function cputurn($symbol)
    {
        //Test to check if board is not full
        $count = 0;
        for($i = 0; $i < 3; $i++)
        {
            for($j = 0; $j < 3; $j++)
            {
                if($_SESSION["board"][$i][$j] == "None")
                {
                    $count += 1;
                }
            }
        }

        if($count > 0)
        {
            $flag = true;
        }
        else
        {
            $flag = false;
        }

        /*Logic to first stop the opponent from winning */
        //flag needed to stop cpu for trying to win by making its own line if opponent is one move short from making a horizontal line
        $flagForCpuLine = true;

        $markers = ["X", "O"];
        //get opponents marker
        $key = array_search($symbol, $markers);
        //deleting the marker from the array that the cpu is using
        array_splice($markers, $key, 1);
        //the remaing in the array would be opponents marker
        $opponent_marker = $markers[0];

        /**Bug fix: the loop below that stops the opponent
        from making horizontal line tries to stop opponent even when it
        itself could win*/
        $chanceforcpuwin = false;
        for($i = 0; $i < 3; $i++)
        {
            $row = array_count_values($_SESSION["board"][$i]);
            if($row[$symbol] == 2)
            {
                $chanceforcpuwin = true;
            }
        }
        /*END OF Bug Fix*/
        //check if opponents horizontal line is been made if yes first stop that
        for($i = 0; $i < 3; $i++)
        {
            //getting data of each row
            $row = array_count_values($_SESSION["board"][$i]);
            if($row[$opponent_marker] == 2 && !$chanceforcpuwin)
            {
                $index = 0;
                //go through that row and make cpu move in that row where move is possible
                while($index < 3)
                {
                    if($_SESSION["board"][$i][$index] == "None")
                    {
                        $_SESSION["board"][$i][$index] = $symbol;
                        //now after this move cpu cant make its own line so stop it from making its line in this move
                        $flagForCpuLine = false;
                        //and stop it from making a random move as well
                        $flag = false;
                    }
                    $index += 1;
                }
            }
        }


        /*logic to make horizontal lines*/
        if($flagForCpuLine)
        {
            for($i = 0; $i < 3; $i++)
            {
                //getting data of each row in form of an array
                $row = array_count_values($_SESSION["board"][$i]);
                //get row that has 1 or 2 Cpu markers and try to make a horizontal line there
                if ($row["None"] <= 2 && $row[$symbol] == 1 || $row[$symbol] == 2 )
                {
                    $index = 0;
                    while($index < 3)
                    {
                        if($_SESSION["board"][$i][$index] == $symbol)
                        {
                            $index += 1;
                        }
                        if($_SESSION["board"][$i][$index] == "None")
                        {
                            $_SESSION["board"][$i][$index] = $symbol;
                            //turn the flag off so that the while loop for random move does not work
                            $flag = false;
                            $index += 1;
                            break;
                        }

                        $index += 1;
                    }
                }
            }
        }


        /*Logic for a random move by cpu on board */
        while($flag)
        {
            $row = rand(0,2);
            $col = rand(0,2);
            if($_SESSION["board"][$row][$col] == "None")
            {
                $_SESSION["board"][$row][$col] = $symbol;
                break;
            }

        }

        //change whose turn it is
        if($_SESSION["turn"] == "X")
        {
            $_SESSION["turn"] = "O";
        }
        else
        {
            $_SESSION["turn"] = "X";
        }

    }




    /*BEGINING OF UNBEATABLE AI */
    //returns an array of array of indexex for empty spaces like [[row,col]]
    function getemptyspaces($board)
    {
        $arr = $board;
        //indexes to be stored as [row, column]
        $spaceindexes = [];
        $rows = count($arr);
        $columns = count($arr[0]);

        for($i = 0; $i < $rows; $i++)
        {
            for($j = 0; $j < $columns; $j++)
            {
                if($arr[$i][$j] == "None")
                {
                    //save the index of empty space
                    $spaceindexes[] = [$i, $j];
                }
            }
        }

        return $spaceindexes;
    }



    function evaluate($board)
    {
        //checking for winning rows
        for($row = 0; $row < 3; $row++)
        {
            if($board[$row][0] == $board[$row][1] && $board[$row][1] == $board[$row][2]
            && $board[$row][0] !== "None" && $board[$row][1] !== "None" && $board[$row][2] !== "None")
            {
                if($board[$row][0] == "X" && $board[$row][1] == "X" && $board[$row][2] == "X")
                {
                    //print("in horizontal");
                    return 10;
                }
                else if($board[$row][0] == "O" && $board[$row][1] == "O" && $board[$row][2] == "O")
                {
                    //print("in horizontal");
                    return -10;
                }
            }
        }
        //checking for vertical lines
        for($col = 0; $col < 3; $col++)
        {
            if($board[0][$col] == $board[1][$col] && $board[1][$col] == $board[2][$col]
            && $board[0][$col] !== "None" && $board[1][$col] !== "None" && $board[2][$col] !== "None")
            {
                if($board[0][$col] == "X" && $board[1][$col] == "X" && $board[2][$col] == "X")
                {
                    //print("in vertical");
                    return 10;
                }
                else if($board[0][$col] == "O" && $board[1][$col] == "O" && $board[2][$col] == "O")
                {
                    //print("in vertical");
                    return -10;
                }
            }
        }
        //checking for daignols
        if($board[0][0] == $board[1][1] && $board[1][1] == $board[2][2]
        && $board[0][0] !== "None" && $board[1][1] !== "None" && $board[2][2] !== "None")
        {
            if($board[0][0] == "X" && $board[1][1] == "X" && $board[2][2] == "X")
            {
                //print("in diagnol");
                return 10;
            }
            else if($board[0][0] == "O" && $board[1][1] == "O" && $board[2][2] == "O")
            {
                //print("in diagnol");
                return -10;
            }
        }
        if($board[0][2] == $board[1][1] && $board[2][0] == $board[0][2]
        && $board[0][2] !== "None" && $board[1][1] !== "None" && $board[2][0] !== "None")
        {
            if($board[0][2] == "X" && $board[1][1] == "X" && $board[2][0] == "X")
            {
                //print("other diagnol");
                return 10;
            }
            else if($board[0][2] == "O" && $board[1][1] == "O" && $board[2][0] == "O")
            {
                //print("in diagnol");
                return -10;
            }
        }

        return 0;
    }













    //function to find if player has a winning line
    /*function evaluate($board)
    {
        //checking for winning rows
        for($row = 0; $row < 3; $row++)
        {
            if($board[$row][0] == $board[$row][1] && $board[$row][1] == $board[$row][2])
            {
                if($board[$row][0] == "X")
                {
                    //print("in horizontal");
                    return 10;
                }
                else if($board[$row][0] == "O")
                {
                    return -10;
                }
            }
        }
        //checking for vertical lines
        for($col = 0; $col < 3; $col++)
        {
            if($board[0][$col] == $board[1][$col] && $board[1][$col] == $board[2][$col])
            {
                if($board[0][$col] == "X" )
                {
                    //print("in vertical");
                    return 10;
                }
                else if($board[0][$col] == "O")
                {
                    return -10;
                }
            }
        }
        //checking for daignols
        if($board[0][0] == $board[1][1] && $board[1][1] == $board[2][2])
        {
            if($board[0][0] == "X" )
            {
                //print("in diagnol");
                return 10;
            }
            else if($board[0][0] == "O")
            {
                return -10;
            }
        }
        if($board[0][2] == $board[1][1] && $board[2][0] == $board[1][1])
        {
            if($board[0][2] == "X")
            {
                //print("other diagnol");
                return 10;
            }
            else if($board[0][2] == "O")
            {
                return -10;
            }
        }

        return 0;
    }*/




    function minimax($board, $turn)
    {
        //print_r($board);
        $game = $board;
        // get the score of current board
        $score = evaluate($game);

        if($score == 10)
        {
            return $score;
        }
        if($score == -10)
        {
            return $score;
        }
        if(count(getemptyspaces($game)) === 0)
        {
            return 0;
        }


        if($turn == "X")
        {
            $availablemoves = getemptyspaces($game);
            $best = -1000;
            foreach ($availablemoves as $move)
            {
                $game[$move[0]][$move[1]] = $turn;
                $best = max($best, minimax($game, "O"));

                //resetting the board
                $game = $board;
            }

            return $best;
        }
        else if($turn == "O")
        {
            $availablemoves = getemptyspaces($game);
            $best = 1000;
            foreach ($availablemoves as $move)
            {
                $game[$move[0]][$move[1]] = $turn;
                $best = min($best, minimax($game, "X"));

                //resetting the board
                $game = $board;
            }

            return $best;
        }
    }




    function bestmove($board, $turn)
    {
        $game = $board;
        //print($turn);
        if($turn == "X")
        {
            $bestval = -1000;
            $bestmove;
            $availablemoves = getemptyspaces($game);
            //print_r($availablemoves);
            foreach($availablemoves as $move)
            {
                //print_r($move);
                $game[$move[0]][$move[1]] = $turn;
                $movevalue = minimax($game, "O");
                //print($movevalue);
                //reset board
                $game = $board;

                if($movevalue > $bestval)
                {
                    $bestval = $movevalue;
                    $bestmove = $move;
                }

                //optimization fix
                if($bestval === 10)
                {
                    break;
                }
            }

            return $bestmove;
        }
        else if($turn == "O")
        {
            $bestval = 1000;
            $bestmove;
            $availablemoves = getemptyspaces($game);

            foreach($availablemoves as $move)
            {
                $game[$move[0]][$move[1]] = $turn;
                $movevalue = minimax($game, "X");
                //reset board
                $game = $board;

                if($movevalue < $bestval)
                {
                    $bestval = $movevalue;
                    $bestmove = $move;
                }

                //optimization fix
                if($bestval === -10)
                {
                    break;
                }
            }

            return $bestmove;
        }
    }

    /*$board = [["X", "X", "O"],
            ["None", "O", "None"],
            ["None", "None", "None"]];

    $move = bestmove($board, "X");
    print_r($move);*/
?>
