<?php
    /*$board = [["X", "X", "O"],
            ["X", "", "O"],
            ["O", "", ""]];*/

    /*$board = [["O", "", ""],
            ["X", "", ""],
            ["X", "", "O"]];*/

    $board = [["", "", ""],
            ["", "", ""],
            ["", "", ""]];


    function isboardfull($arr)
    {
        $rows = count($arr);
        $col = count($arr[0]);

        //the number of empty spaces this board should have
        $emptyspaces = $rows * $col;
        //count for counting empty spaces
        $count = 0;
        foreach($arr as $row)
        {
            for($i = 0; $i < $col; $i++)
            {
                if(!empty($row[$i]))
                {
                    $count += 1;
                }
            }
        }

        if($count < $emptyspaces)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //returns an array of array of indexex for empty spaces like [[row,col]]
    function getemptyspaces($arr)
    {
        //indexes to be stored as [row, column]
        $spaceindexes = [];
        $rows = count($arr);
        $columns = count($arr[0]);

        for($i = 0; $i < $rows; $i++)
        {
            for($j = 0; $j < $columns; $j++)
            {
                if(empty($arr[$i][$j]))
                {
                    //save the index of empty space
                    $spaceindexes[] = [$i, $j];
                }
            }
        }

        return $spaceindexes;
    }

    //function to find if player has a winning line
    function winning($board , $marker)
    {
        if(($board[0][0] == $marker && $board[0][1] == $marker && $board[0][2] == $marker)
        || ($board[1][0] == $marker && $board[1][1] == $marker && $board[1][2] == $marker)
        || ($board[2][0] == $marker && $board[2][1] == $marker && $board[2][2] == $marker)
        || ($board[0][0] == $marker && $board[1][0] == $marker && $board[2][0] == $marker)
        || ($board[0][1] == $marker && $board[1][1] == $marker && $board[2][1] == $marker)
        || ($board[0][2] == $marker && $board[1][2] == $marker && $board[2][2] == $marker)
        || ($board[0][0] == $marker && $board[1][1] == $marker && $board[2][2] == $marker)
        || ($board[0][2] == $marker && $board[1][1] == $marker && $board[2][0] == $marker))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function minmax($game, $turn)
    {
        print_r($game);
        print("Turn:{$turn} \n");
        //$data = [];
        //$gameBoard = $game;
        //get empty spaces
        $availablemoves = getemptyspaces($game);

        //print("Available Moves\n");
        //print_r($availablemoves);
        /*base cases*/
        //if X wins
        if(winning($game, "X"))
        {
            return ["score" => 10];
        }
        else if(winning($game, "O"))
        {
            return ["score" => -10];
        }
        //else if(empty(getemptyspaces($game)))
        //{
            //return ["score" => 0];
        //}
        else if(count(getemptyspaces($game)) === 0)
        {
            return ["score" => 0];
        }

        $data  = [];

        foreach($availablemoves as $move)
        {
            //$data = [];
            //$score = -1000;
            //creating another gameboard in order to preserve the original state so that each move gets the original gameboard
            //reset the board to original
            $gameBoard = $game;

            print("Move");
            print_r($move);
            //making that move
            $gameBoard[$move[0]][$move[1]] = $turn;

            //TEST
            $test = ["move" => $move];

            if($turn == "X")
            {
                //$score = -1000;
                $score =minmax($gameBoard, "O");
            }
            else
            {
                //$score = 1000;
                $score = minmax($gameBoard, "X");
            }

            //TEST
            //$test = ["move" => $move];
            //TEST
            $test2 = ["score" => $score["score"]];
            $result = array_merge($test, $test2);
            array_push($data, $result);


            //reset board
            $gameBoard = $game;

            //print("Score in X:{$score}\n");
            print("SCORES DATA\n");
            print_r($score);
            print("RESULT FOR EACH ITERATION");
            print_r($result);
            if($turn == "X" && $score["score"] == 10)
            {
                break;
            }
            elseif($turn == "O" && $score["score"] == -10)
            {
                break;
            }
        }

        print("END PRINT!!!!");
        print_r($data);
        //looping through the data
        $bestmove = "";
        if($turn == "X")
        {
            $bestscore = -1000;
            foreach($data as $dataentry)
            {
                if($dataentry["score"] > $bestscore && $dataentry["score"] > -10 && $dataentry["score"] != 1000)
                {
                    $bestscore = $dataentry["score"];
                    $bestmove = $dataentry["move"];
                    print("BEST SCORE FOR X IS {$bestscore}\n");

                }
            }
            //return ["move" => $bestmove, "score" => $bestscore];
        }
        else
        {
            $bestscore = 1000;
            foreach($data as $dataentry)
            {
                if($dataentry["score"] < $bestscore && $dataentry["score"] < 10 && $dataentry["score"] != -1000)
                {
                    $bestscore = $dataentry["score"];
                    $bestmove = $dataentry["move"];
                    print("BEST SCORE FOR O IS {$bestscore}\n");


                }

            }
            //return ["move" => $bestmove, "score" => $bestscore];
        }


        return ["move" => $bestmove, "score" => $bestscore];

    }

    while(isboardfull($board) )
    {
        print_r($board);
        print("Input:");
        $input= trim(fgets(STDIN));
        $move = str_split($input);

        $board[$move[0]][$move[1]] = "X";
        print(winning($board, "X"));
        if(winning($board, "X")  || isboardfull($board) == false)
        {
            print_r($board);
            break;
        }

        //TEST cpu
        $cpu = minmax($board, "O");
        $board[$cpu["move"][0]][$cpu["move"][1]] = "O";
        print_r($board);

        if(winning($board, "O"))
        {
            break;
        }
    }


    //print(count(getemptyspaces($board)));
    //print_r(minmax($board, "O"));
    //$bestmoves
    //print(isboardfull($board));







    function factorial($n)
    {
        print("N is now:{$n}\n");
        if($n == 1)
        {
            return 1;
        }
        $val = $n * factorial($n -1);
        print($val . "\n");
        return $val;
    }

    //print(factorial(5));
    /*$data = [];
    $data []= ["anything" => "val", "something" => "someval"];
    $data []= ["anything" => "val", "something" => "someval"];

    print_r($data);*/

    /*$data = [];
    $data []= ["anything" => "val", "something" => "someval"];
    $data []= ["anything" => "val", "something" => "someval"];
    //array_push($data[0],["anotherthing" => "VAL"]);
    $data[0]["anotherthing"] = "VAL";
    print_r($data);*/

    function arrayfactorial($n, $array)
    {
        $data = $array;
        print("Array at start:");
        print_r($array);
        if($n == 1)
        {
            return 1;
        }
        $val = $n * arrayfactorial($n - 1, $data)[count($data) - 1];
        $data [] = $val;
        print("Val is {$val}");
        print("Array at End:");
        print_r($data);
        return $data[count($data) - 1];
    }

    //print(arrayfactorial(5, ["a", "b"]));
?>
